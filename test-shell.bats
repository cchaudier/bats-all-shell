@test "Test sh" {
    which sh
}

@test "Test ksh" {
    which ksh
}

@test "Test bash" {
    which bash
}

@test "Test csh" {
    which csh
}

@test "Test zsh" {
    which zsh
}
